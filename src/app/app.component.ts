import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

interface LanguageOption {
  value: string;
  alias: string;
}

const DEFAULT_LANGUAGE = 'en';
const LANGUAGE_OPTIONS: LanguageOption[] = [
  { value: 'en', alias: 'English' },
  { value: 'fi', alias: 'Suomi' },
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  languages: LanguageOption[] = LANGUAGE_OPTIONS;
  currentLanguage = DEFAULT_LANGUAGE;

  words: string[] = [];

  username = '';
  helloMessage = '';

  constructor(private translateService: TranslateService) {
    this.translateService.setDefaultLang(DEFAULT_LANGUAGE);
    this.loadLanguage();
    this.loadTranslations();
    this.translateService.use(this.currentLanguage);
  }

  loadLanguage() {
    this.currentLanguage = localStorage.getItem('lang') || DEFAULT_LANGUAGE;
  }

  setLanguage(lang: string) {
    this.currentLanguage = lang;
    this.translateService.use(lang);
    localStorage.setItem('lang', lang);
  }

  getLanguage() {
    const index = this.languages.findIndex(lang => lang.value === this.currentLanguage)
    return this.languages[index].alias.toLowerCase();
  }

  loadTranslations() {

    // // hae 'title'-käännös
    // this.translateService.get('title').subscribe((translation) => {
    //   this.text = translation;
    // });

    // // hae käännökset 'button'-avaimen alla
    // this.translateService.get('button').subscribe((translations) => {
    //   this.buttonOpen = translations.open;
    //   this.buttonClose = translations.close;
    // });
    // <span translate [translateParams]="{ name: username }">content.dynamic_translate.hello_message</span>
    // <span>{{ 'content.dynamic_translate.hello_message' | translate:{ name: username } }}</span>
    // this.translateService.get('content.dynamic_translate.hello_message', { name: this.username})

    this.translateService.get('sidebar.dynamic_translate.hello_message', { name: this.username })
      .subscribe((translation) => {
        this.helloMessage = translation;
      });

    // get all the words under key words in translation files
    const example_path = 'sidebar.example_words.words';
    this.translateService.get(example_path).subscribe((translations) => {
      if (translations) {
        this.words = Object.keys(translations).map((key) => `${example_path}.${key}`);
      }
    });
  }
}
