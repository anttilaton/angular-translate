import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface LanguageOption {
  value: string,
  alias: string
};

const LANGUAGE_OPTIONS: LanguageOption[] = [
  { value: 'en', alias: 'English' },
  { value: 'fi', alias: 'Suomi' },
];

export class CustomTranslateLoader extends TranslateHttpLoader {
  supportedLanguages: string[] = LANGUAGE_OPTIONS.map(option => option.value);
  defaultLanguage = 'en';

  constructor(http: HttpClient) {
    super(http, '/assets/lang/', '.json');
  }

  getTranslation(language: string): Observable<object> {
    // language is not supported
    if (this.supportedLanguages.indexOf(language) === -1) {
      language = this.defaultLanguage;
    }

    return super.getTranslation(language);
    // return super.getTranslation(language).pipe(
    //   map((json) => {
    //     console.log(json);
    //     return this.formatLocales(json);
    //   })
    // );
  }

  // formatLocales(json: any): any {
  //   console.log(json);
  //   return Object.keys(json).reduce((obj, key) => {
  //     // console.log(json, obj, key);
  //     if (typeof json[key].text === 'string' && typeof json[key].comment === 'string') {
  //       obj[key] = json[key].text;
  //     } else {
  //       obj[key] = this.formatLocales(json[key]);
  //     }
  //     return obj;
  //   }, {});
  // }
}